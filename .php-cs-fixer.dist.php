<?php

$finder = (new PhpCsFixer\Finder())
    ->in([__DIR__ . '/src', __DIR__ . '/bin'])
    ->name('run')
;

return (new PhpCsFixer\Config())
    ->setRules([
        '@PER-CS' => true,
        '@PHP84Migration' => true,
        'no_unused_imports' => true,
        'type_declaration_spaces' => true,
    ])
    ->setFinder($finder)
;

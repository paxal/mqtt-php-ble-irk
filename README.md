# MQTT PHP BLE IRK

Oh my god…, so much capital letters!

This project is a helper to help detect phones for low-CPU ble devices, like
Shelly modules using Shelly Scripts.

## The problem

Phones, watched, and other devices do not advertise their real Bluetooth MAC address.
Instead, they generate random MAC addresses every X minutes so that your device
cannot be tracked.

But these MAC have an hidden signature, and using a special key, you can
identify those devices.

The Key is the Identity Resolving Key (IRK). Given a public MAC address, we can
check if it has been generated via an IRK. The IRK is a secret that can be found
on Apple MacOS and Linux devices.

<details>
<summary>On Linux</summary>
Bluez saves IRK on /var/lib/bluetooth folder or similar.

```
# 11:11:11:22:22:22 is the bluetooth adapter MAC address
# 33:33:33:44:44:44 is the real device MAC address
# /var/lib/bluetooth/11:11:11:22:22:22/33:33:33:44:44:44/info

# This is the IRK
[IdentityResolvingKey]
Key=0123456789ABCDEF0123456789ABCDEF
```
</details>

## Configuration and run

### CSV File Format
Create a CSV file with your devices, like the keys.example.csv in this project.
Change mac addresses, names and keys.

### Run

Then the project:
```bash
# Use ./bin/run if you run the project from the sources 
./mqtt-php-ble-irk.phar 'mqtt://localhost/php-ble-irk' 'keys.example.csv'
```

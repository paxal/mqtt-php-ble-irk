<?php

declare(strict_types=1);

namespace Paxal\MqttPhpBleIrk\MQTT;

use PhpMqtt\Client\ConnectionSettings;
use PhpMqtt\Client\Contracts\MqttClient;
use Psr\Log\LoggerInterface;

final class ClientFactory
{
    private const DEFAULT_PATH = 'php-ble-irk';
    private const DEFAULT_HOST = '127.0.0.1';
    private const DEFAULT_PORT = 1883;

    public function __construct(private readonly LoggerInterface $logger) {}

    public function create(string $url): MqttClient
    {
        [
            'host' => $host,
            'port' => $port,
        ] = parse_url($url) + ['host' => self::DEFAULT_HOST, 'port' => self::DEFAULT_PORT];
        $this->logger->notice('Will Connect to MQTT ' . $host . ':' . $port);
        $mqtt = new \PhpMqtt\Client\MqttClient($host, $port);

        return $mqtt;
    }

    public function connect(string $url, MqttClient $mqttClient, Handler $handler): void
    {
        [
            'user' => $user,
            'pass' => $pass,
            'path' => $path,
        ] = parse_url($url) + ['user' => null, 'pass' => null, 'path' => self::DEFAULT_PATH];
        $path = ltrim($path, '/');

        $connectionSettings = (new ConnectionSettings())
            ->setUsername($user)
            ->setPassword($pass);
        $mqttClient->connect($connectionSettings);
        $this->logger->notice('MQTT Connected');

        $mqttClient->subscribe($path, $handler->__invoke(...));
        $this->logger->notice('MQTT Subscribed to ' . $path);
    }
}

<?php

declare(strict_types=1);

namespace Paxal\MqttPhpBleIrk\MQTT;

use Paxal\MqttPhpBleIrk\BLE\IRKRepository;
use Paxal\MqttPhpBleIrk\BLE\Mac;
use PhpMqtt\Client\Contracts\MqttClient;
use Psr\Log\LoggerInterface;

final class Handler
{
    public function __construct(
        private readonly MqttClient $mqttClient,
        private readonly IRKRepository $repository,
        private readonly ?LoggerInterface $logger = null,
    ) {}

    /**
     * @psalm-suppress UnusedParam
     */
    public function __invoke(string $topic, string $payload): void
    {
        /** @var mixed $payload */
        $payload = json_decode($payload, true);
        if (!\is_array($payload)) {
            $this->logger?->error('Unable to decode payload');
            return;
        }

        $mac = $payload['mac'] ?? null;
        if (!Mac::isValid($mac)) {
            $this->logger?->error('Invalid MAC address given');
            return;
        }

        $mac = Mac::normalize($mac);
        $responseTopic = $payload['user'] ?? null;
        if (!\is_string($responseTopic)) {
            $this->logger?->error('Invalid response topic');
            return;
        }

        $device = $this->repository->resolve($mac);
        $responsePayload = ['ok' => true, 'mac' => $mac, 'device' => $device];

        $this->logger?->info(json_encode(['topic' => $responseTopic, 'payload' => $responsePayload]));
        $this->mqttClient->publish($responseTopic, json_encode($responsePayload));
    }
}

<?php

declare(strict_types=1);

namespace Paxal\MqttPhpBleIrk\BLE;

final class IRKRepository
{
    private const PAD = "\0\0\0\0\0\0\0\0\0\0\0\0\0";
    private const BLE_MAC_CHECK_ALG = 'aes-128-ecb';
    private const HEADERS = ['irk', 'mac', 'name'];

    public function __construct(
        /** @var list<Device> */
        private readonly array $devices
    ) {}

    public function resolve(string $mac): ?Device
    {
        $macBin = hex2bin(str_replace(':', '', $mac));
        $resolvable = (\ord($macBin) & 0xc0) === 0x40;

        foreach ($this->devices as $device) {
            if ($device->mac === $mac) {
                return $device;
            }

            if (!$resolvable) {
                continue;
            }

            if ($device->irk === null) {
                continue;
            }

            $irk = $device->irk;
            // Test IRK and reversed-IRK, depending how IRK was retrieved
            // - BLUEZ stores IRK as reversed string
            foreach ([$irk, strrev($irk)] as $testedIrk) {
                $encrypted = openssl_encrypt(
                    data: self::PAD . substr($macBin, 0, 3),
                    cipher_algo: self::BLE_MAC_CHECK_ALG,
                    passphrase: $testedIrk,
                    options: OPENSSL_RAW_DATA,
                );
                if ($encrypted === false) {
                    throw new \RuntimeException('Unable to encrypt data, incorrect mac or key');
                }

                if (substr($encrypted, 13, 3) === substr($macBin, 3)) {
                    return $device;
                }
            }
        }

        return null;
    }

    public static function createFromCSVFile(string $filename): self
    {
        $fp = fopen($filename, 'r');
        if (!\is_resource($fp)) {
            throw new \InvalidArgumentException('Unable to read CSV from file ' . $filename);
        }

        $headers = fgetcsv($fp);
        asort($headers);
        if (array_values($headers) !== self::HEADERS) {
            throw new \InvalidArgumentException('CSV has wrong header, should be mac,name,irk');
        }
        ksort($headers);

        $devices = [];
        while (!\feof($fp) && $line = fgetcsv($fp)) {
            // Skip empty line
            if ($line === [null]) {
                continue;
            }
            if (\count($line) !== \count($headers)) {
                throw new \InvalidArgumentException('CSV badly formatted, not same column count');
            }

            /** @var array{mac:string,name:string,irk:string} $deviceInfo */
            $deviceInfo = array_combine($headers, $line);

            $binaryIrk = self::parseIrk($deviceInfo['irk']);
            $mac = $deviceInfo['mac'];
            /** @psalm-suppress DocblockTypeContradiction */
            if (!Mac::isValid($mac)) {
                throw new \InvalidArgumentException('Invalid mac address given: ' . $mac);
            }
            $mac = Mac::normalize($mac);

            $devices[] = new Device($mac, $deviceInfo['name'], $binaryIrk);
        }

        return new self($devices);
    }

    private static function parseIrk(string $irk): ?string
    {
        $irk = trim($irk);
        if ($irk === '') {
            return null;
        }

        $binaryIrk = hex2bin($irk);
        if ($binaryIrk === false) {
            throw new \InvalidArgumentException('IRK is not valid');
        }
        if (\strlen($binaryIrk) !== 16) {
            throw new \InvalidArgumentException('IRK is not 16-bytes hex string');
        }

        return $binaryIrk;
    }
}

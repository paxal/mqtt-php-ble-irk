<?php

declare(strict_types=1);

namespace Paxal\MqttPhpBleIrk\BLE;

final class Device implements \JsonSerializable
{
    public function __construct(
        public readonly string $mac,
        public readonly string $name,
        public readonly ?string $irk,
    ) {}

    public function jsonSerialize(): array
    {
        return [
            'mac' => $this->mac,
            'name' => $this->name,
        ];
    }
}

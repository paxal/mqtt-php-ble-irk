<?php

declare(strict_types=1);

namespace Paxal\MqttPhpBleIrk\BLE;

final class Mac
{
    /**
     * @param mixed $mac
     * @return bool
     * @psalm-assert-if-true string $mac
     */
    public static function isValid(mixed $mac): bool
    {
        if (!\is_string($mac)) {
            return false;
        }

        if (!preg_match('@^[0-9a-fA-F]{2}(:[0-9a-fA-F]{2}){5}$@', $mac)) {
            return false;
        }

        return true;
    }

    public static function normalize(string $mac): string
    {
        return strtolower($mac);
    }
}

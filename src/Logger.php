<?php

declare(strict_types=1);

namespace Paxal\MqttPhpBleIrk;

use Psr\Log\AbstractLogger;
use Psr\Log\LogLevel;

final class Logger extends AbstractLogger
{
    private const LOG_LEVEL_MAP = [
        LogLevel::EMERGENCY => LOG_EMERG,
        LogLevel::ALERT => LOG_ALERT,
        LogLevel::CRITICAL => LOG_CRIT,
        LogLevel::ERROR => LOG_ERR,
        LogLevel::WARNING => LOG_WARNING,
        LogLevel::NOTICE => LOG_NOTICE,
        LogLevel::INFO => LOG_INFO,
        LogLevel::DEBUG => LOG_DEBUG,
    ];

    private int $logLevel;

    public function __construct(string $logLevel = LogLevel::INFO)
    {
        $this->logLevel = self::LOG_LEVEL_MAP[$logLevel];
    }

    public function log($level, \Stringable|string $message, array $context = []): void
    {
        if (!\is_string($level)) {
            throw new \BadMethodCallException('Level is not a string');
        }
        $int = self::LOG_LEVEL_MAP[$level] ?? LOG_EMERG;
        if ($int > $this->logLevel) {
            return;
        }

        $msg = "[$level] $message " . json_encode($context);
        if ($int > LOG_INFO) {
            fputs(STDERR, $msg);
        } else {
            // @phpcs
            fputs(STDOUT, $msg . PHP_EOL);
        }
    }
}

BOX_VERSION ?= 4.5.1
BOX_PHAR = box-$(BOX_VERSION).phar
PHP ?= php
COMPOSER ?= $(shell which composer)

test: phpcs phpcsfixer psalm phpunit

phpcs:
	$(PHP) ./vendor/bin/phpcs src bin

phpcsfixer:
	$(PHP) ./vendor/bin/php-cs-fixer fix --diff

psalm:
	$(PHP) ./vendor/bin/psalm

phpunit:
	$(PHP) ./vendor/bin/phpunit tests

phar: mqtt-php-ble-irk.phar

$(BOX_PHAR):
	curl --fail -Lo box-$(BOX_VERSION).phar "https://github.com/box-project/box/releases/download/$(BOX_VERSION)/box.phar"

mqtt-php-ble-irk.phar: $(BOX_PHAR)
	rm -fr vendor/
	$(PHP) $(COMPOSER) install --no-dev -a --prefer-dist
	$(PHP) $(BOX_PHAR) compile

.PHONY: mqtt-php-ble-irk.phar

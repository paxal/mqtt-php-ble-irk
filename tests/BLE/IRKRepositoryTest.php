<?php

declare(strict_types=1);

namespace Paxal\MqttPhpBleIrk\Tests\BLE;

use Paxal\MqttPhpBleIrk\BLE\Device;
use Paxal\MqttPhpBleIrk\BLE\IRKRepository;
use PHPUnit\Framework\TestCase;

final class IRKRepositoryTest extends TestCase
{
    /**
     * @return iterable<array{list<Device>,string,?string}>
     */
    public static function getData(): iterable
    {
        $device = new Device('11:22:33:44:55:66', 'Phone', str_repeat("\0", 16));
        yield '1 device, resolvable, match' => [[$device], '40:01:02:0a:c4:a6', '11:22:33:44:55:66'];
        yield '1 device, resolvable, other match' => [[$device], '40:02:03:d2:74:ce', '11:22:33:44:55:66'];
        yield '1 device, resolvable, no match' => [[$device], '40:00:00:d2:74:ce', null];
        yield '1 device, not resolvable, no match' => [[$device], '00:01:ff:a0:3a:76', null];
        yield '1 device, not resolvable, again no match' => [[$device], '77:77:77:88:88:88', null];

        $publicDevice = new Device('77:77:77:88:88:88', 'Public', null);
        yield '2 devices, resolvable, match' => [[$device, $publicDevice], '40:01:02:0a:c4:a6', '11:22:33:44:55:66'];
        yield '2 devices, not resolvable, match' => [[$device, $publicDevice], '77:77:77:88:88:88', '77:77:77:88:88:88'];
        yield '2 devices, mac resolvable, no match' => [[$device, $publicDevice], '40:00:00:d2:74:ce', null];
        yield '2 devices, mac not resolvable, no match' => [[$device, $publicDevice], '00:01:ff:a0:3a:76', null];
    }

    /**
     * @dataProvider getData
     *
     * @param list<Device> $devices
     */
    public function test(array $devices, string $mac, ?string $expectedDeviceMac): void
    {
        $repository = new IRKRepository($devices);

        self::assertSame($expectedDeviceMac, $repository->resolve($mac)?->mac);
    }
}

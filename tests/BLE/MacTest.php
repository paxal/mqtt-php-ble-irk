<?php

declare(strict_types=1);

namespace Paxal\MqttPhpBleIrk\Tests\BLE;

use Paxal\MqttPhpBleIrk\BLE\Mac;
use PHPUnit\Framework\TestCase;

final class MacTest extends TestCase
{
    /**
     * @return iterable<array{string, bool}>
     */
    public static function getData(): iterable
    {
        yield ['00:00:00:00:00:00', true];
        yield ['af:bc:ed:98:14:23', true];
        yield ['af:Bc:eD:98:14:23', true];

        yield ['ag:bc:ed:98:14:23', false];
        yield ['aF:bc:ed:98:14;23', false];
    }

    /**
     * @dataProvider getData
     */
    public function testIsValid(string $mac, bool $expected): void
    {
        self::assertSame($expected, Mac::isValid($mac));
    }
}
